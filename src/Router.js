import React from 'react';
import { Router, Scene, Actions } from 'react-native-router-flux';
import NewsView from './components/NewsView';
import DetailNewsView from './components/DetailNewsView'

const RouterComponent = () => {
        return(
            <Router> 
                    <Scene key="main" hideNavBar>
                        <Scene key="root">
                            <Scene key="newsView" component={NewsView} title="News" />
                        </Scene>

                        <Scene 
                            onExit={() => Actions.root()}
                            key="detailNewsView" 
                            component={DetailNewsView} 
                            title="Detail News"   
                        />           
                    </Scene>
            </Router>
        )
}

export default RouterComponent;
